class SimpleDotComTestDrive {
	public static void main (String[] args) {

		SimpleDotCom dot = new SimpleDotCom();
		int[] location = {2,3,4};
		dot.setLocationCells(location);

		String userGuess = "2";
		String result = dot.checkYourself(userGuess);
		String testResult = "Fail";

		if (result.equals("Get")) {
			testResult = "Passed";
		}
		System.out.println(testResult);
	}
}