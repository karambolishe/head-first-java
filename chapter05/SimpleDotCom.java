class SimpleDotCom {
	int [] locationCells;
	int numOfHits = 0;
	boolean [] countsOfGet = new boolean[7];
	
	public void setLocationCells(int[] loc) {
		locationCells = loc;
	}
	
	public String checkYourself(String stringGuess) {
		int guess =  Integer.parseInt(stringGuess);
		String result = "Milk";
		for (int cell : locationCells) {
			if (cell == guess) {
				if (countsOfGet[cell] == false) {
					countsOfGet[cell] = true;
				} else {
					System.out.println("Duplicate is not allowed. Please enter other number.");
					break;
				}

				result = "Get";
				System.out.println(result);
				numOfHits++;
				break;
			}
		}

		if (numOfHits == locationCells.length) {
			result = "Killed";
		}
		//System.out.println(result);
		return result;
	}
}