class SimpleDotComGame {
	public static void main(String[] args) {
		int numOfGuesses = 0;

		GameHelper helper = new GameHelper();
		SimpleDotCom dot = new SimpleDotCom();

		int startPosition = (int) (Math.random() * 5);

		int [] location = {startPosition, startPosition + 1, startPosition + 2};
		dot.setLocationCells(location);

		boolean isActive = true;

		while (isActive == true) {
			String guess = helper.getUserInput("Enter your number: ");
			String result = dot.checkYourself(guess);
			
			if (result == "Killed") {
				isActive = false;
				System.out.print("You are win! Your counts try is: ");
				System.out.println(numOfGuesses);
			}
		}
	}	
}