public class MovieTestDrive {
	public static void main (String [] args) {
		Movie one = new Movie();
		one.title = "NikoIII";
		one.genre = "action";
		one.rating  = 9;
		Movie two = new Movie();
		two.title = "Tom&Jerry";
		two.genre = "child";
		two.rating  = 10;
		Movie three = new Movie();
		three.title = "Mr. Bean";
		three.genre = "comedy";
		three.rating  = 10;
		Movie four = new Movie();
		four.title = "dead guy";
		four.genre = "fignya";
		four.rating  = 2;

		one.playIT();
		System.out.println("Title: " + one.title);
		System.out.println("Genre: " + one.genre);
		System.out.println("Rating: " + one.rating);

		two.playIT();
		System.out.println("Title: " + two.title);
		System.out.println("Genre: " + two.genre);
		System.out.println("Rating: " + two.rating);

		two.playIT();
		System.out.println("Title: " + three.title);
		System.out.println("Genre: " + three.genre);
		System.out.println("Rating: " + three.rating);

		two.playIT();
		System.out.println("Title: " + four.title);
		System.out.println("Genre: " + four.genre);
		System.out.println("Rating: " + four.rating);
	}
}