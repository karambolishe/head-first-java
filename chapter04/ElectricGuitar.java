class ElectricGuitar {
	String brand;
	int numOfPickups;
	boolean rockStarUsesIt;

	String getBrand() {
		return brand;
	}

	void setBrand(String aBrand) {
		brand = aBrand;
	}

	int getNumOfPickups() {
		return numOfPickups;
	}

	void setNumOfPickups(int aNumOfPickups) {
		numOfPickups = aNumOfPickups;
	}

	boolean getRockStarUsesIt() {
		return rockStarUsesIt;
	}

	void setGetRockStarUsesIt(boolean aRockStarUsesIt) {
		rockStarUsesIt = aRockStarUsesIt;
	}
}