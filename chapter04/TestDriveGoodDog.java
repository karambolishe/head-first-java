class TestDriveGoodDog {
	public static void main(String[] args) {
	GoodDog one = new GoodDog();
	one.setSize(5);
	GoodDog two = new GoodDog();
	two.setSize(15);
	GoodDog three = new GoodDog();
	three.setSize(100);
	System.out.println("First dog has: " + one.getSize() + "kg");
	one.bark();
	System.out.println("Two dog has: " + two.getSize() + "kg");
	two.bark();
	System.out.println("Other dog has: " + three.getSize() + "kg");
	three.bark();
	}
}