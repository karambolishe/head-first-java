public class PhraseMatic {
	public static void main (String[] args) {
		String[] wordListOne = {"принцип", "учёта", "более","широкое","расизма",
		"Так","что","расизм","это","людей","строго","группы","расами","связи","между",
		"чертами","чертами","моралью","также","одних","над","другими","теорий","практике",
		"находит","своё","политике","расовой","права"};

		String[] wordListTwo = {"дисциплина","называется","программная","инженерия","постановка",
		"проектирование","программы","построение","алгоритмов","разработка","написание","тестирование",
		"программы","испытания","программы","документирование","настройка","конфигурирование",
		"доработка","сопровождение"};

		String[] wordListThree = {"анализирует","содержимое","сохраняет","некотором","специальном",
		"поисковой","принадлежит","отправляется","следующие","Владельцы","поисковых","ограничивают",
		"проникновения","максимальный","сканируемого","оказаться","полностью","поисковой","существуют",
		"называемые","простукивают","проиндексированный","определить"};

		int oneLength = wordListOne.length;
		int twoLength = wordListTwo.length;
		int threeLength = wordListThree.length;

		int rand1 = (int) (Math.random() * oneLength);
		int rand2 = (int) (Math.random() * twoLength);
		int rand3 = (int) (Math.random() * threeLength);

		String phrase = wordListOne[rand1] + wordListTwo[rand2] + wordListThree[rand3];

		System.out.println("Все что тебе нужно это - " + phrase);
	}
}