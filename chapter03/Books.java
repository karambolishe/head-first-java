class Books {
	String title;
	String author;

	public static void main(String[] args) {
		Books book1 = new Books();
		book1.title = null;
		book1.author = null;

		Books [] myBooks = new Books[3];
		myBooks[0] = new Books();
		myBooks[1] = new Books();
		myBooks[2] = new Books();
		int x = 0;

		myBooks[0].title = "Java benefit";
		myBooks[1].title = "Java Getsbi";
		myBooks[2].title = "Dictionary of Java";
	
		myBooks[0].author = "Bob";
		myBooks[1].author = "Suy";
		myBooks[2].author = "Yan";
	
		while (x < 3) {
			System.out.print(myBooks[x].title);
			System.out.print(", author: ");
			System.out.println(myBooks[x].author);
			x = x + 1;
		}
	}
}